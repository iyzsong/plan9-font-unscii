<$PLAN9/src/mkhdr
TARG = x
OFILES = main.o
<$PLAN9/src/mkone

all: font

unscii-16-full.hex:
	wget http://viznut.fi/unscii/unscii-16-full.hex

font: $PROG unscii-16-full.hex
	./$PROG unscii-16-full.hex

clean:
	rm -f $CLEANFILES *.16 font
