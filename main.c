#include <u.h>
#include <libc.h>
#include <bio.h>
#include <draw.h>
#include <memdraw.h>


typedef struct Glyph {
    int width;
    uchar bits[32];
} Glyph;
Glyph glyphs[0x20000];


int d2h(char c)
{
    if (c >= '0' && c <= '9')
        return c - '0';
    if (c >= 'a' && c <= 'f')
        return c - 'a' + 10;
    if (c >= 'A' && c <= 'F')
        return c - 'A' + 10;
    return 0;
}


void readglyphs(char *file)
{
    char *line, *s, *p;
    Biobuf *bp;
    long n, w;
    int i;

    memset(glyphs, 0, sizeof(glyphs));
    bp = Bopen(file, OREAD);
    if (bp == nil)
        sysfatal("can't open %s: %r", file);

    while ((line = Brdline(bp, '\n')) != nil) {
        s = strtok(line, ":");
        n = strtol(s, nil, 16);
        s = strtok(nil, "\n");
        w = strlen(s);
        if (w == 32)
            glyphs[n].width = 8;
        if (w == 64)
            glyphs[n].width = 16;
        p = s;
        for (i = 0; i < w / 2; ++i) {
            glyphs[n].bits[i] = d2h(p[0])*16 + d2h(p[1]);
            p += 2;
        }
    }
    Bterm(bp);
}

void writefont()
{
    int fd = create("font", OWRITE, 0644);
    fprint(fd, "16 16\n");
    for (int i = 0; i < 0x20000; i += 0x100)
        fprint(fd, "0x%05x 0x%05x %05x.16\n", i, i+0xff, i);
    close(fd);
}

void main(int argc, char *argv[])
{
    memimageinit();
    readglyphs(argv[1]);
    writefont();

    Subfont subfont;
    Fontchar *info = malloc(257 * sizeof(Fontchar));
    subfont.name = "unscii";
    subfont.n = 256;
    subfont.height = 16;
    subfont.ascent = 16;
    subfont.info = info;
    subfont.ref = 0;

    for (int i = 0; i < 0x20000; i += 0x100) {
        Rectangle r = Rect(0, 0, 0, 0);
        Rectangle rr = Rect(0, 0, 0, 16);
        for (int j = i; j < i + 0x100; j += 1) {
            r.max.x += glyphs[j].width;
        }
        if (r.max.x == 0)
            r = Rect(0, 0, 1, 1);
        else
            r.max.y = 16;

        Memimage *img = allocmemimage(r, GREY1);

        for (int j = 0; j < 257; j += 1) {
            if (glyphs[i+j].width == 0) {
                info[j].x = (j == 0) ? 0 : info[j-1].x;
                info[j].top = 0;
                info[j].bottom = 0;
                info[j].left = 0;
                info[j].width = 0;
                continue;
            }

            info[j].x = (j == 0) ? 0 : glyphs[i+j].width + info[j-1].x;
            info[j].top = 0;
            info[j].bottom = 16;
            info[j].left = 0;
            info[j].width = glyphs[i+j].width;

            rr.max.x = rr.min.x + info[j].width;
            loadmemimage(img, rr, glyphs[i+j].bits, glyphs[i+j].width * 2);
            rr.min.x += info[j].width;
        }

        char file[9];
        snprint(file, 9, "%05x.16", i);
        int fd = create(file, OWRITE, 0644);
        writememimage(fd, img);
        writesubfont(fd, &subfont);
        close(fd);
        freememimage(img);
    }

    free(info);
    exits(0);
}
